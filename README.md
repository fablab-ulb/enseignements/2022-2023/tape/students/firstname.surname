# Journal de bord d'Arsène Lupin

## Qui suis-je ?

Présentez-vous en quelques mots, vos motivations..

## 26 septembre 2022

Aujourd'hui j'ai appris les bases du [Markdown](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown) et comment compresser des images avec des images avec [GIMP](https://www.gimp.org/) avant des les uploader dans mon journal de bord.

* [Open-source lab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/blob/9ca42c056c4371315ecdac35195d28098be02f0b/vade-mecum/open-source-lab.md)
* Communauté [Microcosmos](https://microcosmos.foldscope.com/)

![image d'un foldscope](img/foldscope.jpg)

test

### Ted talk de Manu Prakash

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h8cF5QPPmWU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## 28 septembre 2022

*Lorem ipsum* dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in **reprehenderit in voluptate** velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Qui voluptas nisi et sed ipsam voluptatem. Quam ***voluptas voluptatibus*** debitis recusandae molestiae excepturi nulla. Dolores sunt accusantium dicta fugiat eius nam dolor. Ea possimus quae ipsam ea ipsa non cumque.

Sapiente perferendis voluptatem aut laboriosam. Omnis ipsam voluptatum est quis reiciendis velit dolorem. Itaque modi ad et suscipit. Facere eius et provident eaque qui velit.

Fuga dignissimos beatae tenetur et facilis non. Et debitis occaecati necessitatibus. Qui vitae est omnis. Ea quia error fugiat dolorum. Molestiae dignissimos odio temporibus amet culpa rerum eius et. Voluptas similique quia neque dignissimos.

Quos quas esse quae incidunt aut sequi exercitationem ea. Eveniet exercitationem repellendus quis sequi aut et non tenetur. Voluptatem aut et voluptatibus aut blanditiis mollitia suscipit.

Sint vitae reprehenderit sunt veniam. Ipsam sequi at minus. In vel rerum amet.


